��    �      4  �   L        �    J   �    �  
    4     :   B     }     �     �     �     �     �     �  "   �  1   �     (     .     >     J     V     ^     b     s     �     �     �     �     �     �  (   �       Y        o     �     �  %   �  /   �  /   �  	         *     1     ?     E  !   M     o     v     �     �  0   �  /   �  
     	             ,     E     V     \     i     r     �     �  	   �     �  =   �  >   �  
   +     6     =     F     O  	   [  	   e     o     w     |     �     �     �     �     �     �     �     �     �     �     �                          '     7     <     I     W     ^     m     }     �     �     �     �  	   �     �     �     �       
        %  R   <     �     �     �     �     �     �     �     �  /     X   7     �     �  	   �     �     �     �     �     �     �     �  �  �  2  �  �   �  �  �  �  1  h   �  d   H   	   �      �      �      �   .   �      !  $   !  )   <!  B   f!     �!  (   �!  
   �!     �!  
   "  
   "     "     <"      ["     |"     �"     �"     �"     �"  G   #     ]#  �   n#  !   $     :$     R$  N   a$  a   �$  _   %     r%     �%     �%     �%     �%  7   �%     �%     
&  (   &     <&  q   I&  T   �&     '     /'     D'  1   ^'     �'     �'     �'     �'     �'  
   �'     �'     (     '(  d   A(  m   �(     )  
   0)     ;)     L)  #   b)  !   �)     �)     �)     �)     �)     �)  (   *     <*     T*     m*     y*      �*     �*     �*     �*     �*     �*     +     +     1+  $   @+     e+     t+     �+  
   �+     �+     �+     �+  1   ,  5   D,  $   z,     �,     �,     �,     �,  1   �,     "-     /-  .   C-  �   r-      �-     .  =   &.  %   d.     �.     �.  /   �.  5   �.  L   /  �   b/     �/  !   	0     +0     ;0     K0     W0     i0     u0     �0     �0         A                  ,   X   1      k                             [   :   j       "   Y   0          P   	   M   2   t       G       8   
               s   z   .              )       '   !   ~   �   E   4       (   +       <       �      r   $   /       -   6      ^   _   *   T       %   7      q   B   ?   n      x   3   l   Q   w   J       R       5       S      �      D   @   }   =       C         W         >   U   #       y   b      e   L   c   F   h   N   O       i      Z          `              H   v                  �       g   m   o       u      p           \   &   {       V   I   ]       K   |   f       9                              a   d              ;           "Exec": Program to execute, possibly with arguments. The Exec key is required
if DBusActivatable is not set to true. Even if DBusActivatable is true, Exec
should be specified for compatibility with implementations that do not
understand DBusActivatable. 

Please see
http://standards.freedesktop.org/desktop-entry-spec/desktop-entry-spec-latest.html#exec-variables
for a list of supported arguments. "GenericName": Generic name of the application, for example "Web Browser". "NotShowIn": A list of strings identifying the environments that should not
display a given desktop entry. Only one of these keys, either OnlyShowIn or
NotShowIn, may appear in a group.

Possible values include: GNOME, KDE, LXDE, MATE, Razor, ROX, TDE, Unity, XFCE, Old "OnlyShowIn": A list of strings identifying the environments that should
display a given desktop entry. Only one of these keys, either OnlyShowIn or
NotShowIn, may appear in a group.

Possible values include: GNOME, KDE, LXDE, MATE, Razor, ROX, TDE, Unity, XFCE, Old "Path": The working directory to run the program in. "Terminal": Whether the program runs in a terminal window. 128px 16px 32px 64px <b>Application Details</b> <b>Preview</b> <b>Select an image</b> <big><b>Application Name</b></big> A small descriptive blurb about this application. About About MenuLibre Accessories Action Name Actions Add Add Directory... Add Launcher... Add Separator... Add _Directory Add _Directory... Add _Launcher Add _Launcher... Add _Separator Add or remove applications from the menu Advanced All changes since the last saved state will be lost and cannot be restored automatically. Application Comment Application Name Apply Are you sure you want to delete "%s"? Are you sure you want to delete this separator? Are you sure you want to restore this launcher? Browse… Cancel Category Name Clear Command Copyright © 2012-2014 Sean Davis Delete Description Desktop configuration Development Do you want to read the MenuLibre manual online? Do you want to save the changes before closing? Don't Save Education GNOME application GNOME user configuration GTK+ application Games Generic Name Graphics Hardware configuration Help Hidden Icon Name Icon Selection If you don't save the launcher, all the changes will be lost. If you don't save the launcher, all the changes will be lost.' Image File Images Internet Keywords Menu Editor MenuLibre Move Down Move Up Name New Directory New Launcher New Menu Item New Shortcut Not Shown In OK Office Online Documentation Only Shown In Other Quit Read Online Redo Remove Restore Launcher Revert Run in terminal Save Save Changes Save Launcher Search Search Results Search terms… Select a category Select a working directory... Select an executable... Select an icon… Select an image Separator Settings Show Show debug messages System This Entry This cannot be undone. This launcher has been removed from the system.
Selecting the next available item. Try Exec Undo Use startup notification User configuration WINE Working Directory Xfce menu item Xfce user configuration You do not have permission to delete this file. You will be redirected to the documentation website where the help pages are maintained. _About _Add Separator... _Contents _Delete _Edit _File _Help _Quit _Save column Project-Id-Version: menulibre
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-08-03 13:05-0400
PO-Revision-Date: 2014-04-19 04:31+0000
Last-Translator: Саша Петровић <salepetronije@gmail.com>
Language-Team: српски <српски <xfce-i18n@xfce.org>>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2014-08-04 06:22+0000
X-Generator: Launchpad (build 17147)
Language: sr
 „Exec“: Извршити програм, по потреби са одредницама. Кључ „Exec“ је потребан
ако „DBusActivatable“ није укључено. Чак и ако јесте, Exec
би требало да буде одређен због сагласности са применама које не
разумеју „DBusActivatable“. 

Погледајте
http://standards.freedesktop.org/desktop-entry-spec/desktop-entry-spec-latest.html#exec-variables
за списак подржаних одредница. „Опште име“: Опис које указује на врсту програма, као што је „Веб прегледник“ „Не приказуј у“: Списак ниски које одређују окружења у којима се не
приказује дата ставка површи. Само један од кучева Само приказуј у или
Не приказуј у могу да се приказују у скупу.

Могуће вредности су: GNOME, KDE, LXDE, MATE, Razor, ROX, TDE, Unity, XFCE, Old „Само приказуј у“:  Списак ниски које одређују окружења у којима се 
приказује дата ставка површи. Само један од кучева Само приказуј у или
Не приказуј у могу да се приказују у скупу.

Могуће вредности су: GNOME, KDE, LXDE, MATE, Razor, ROX, TDE, Unity, XFCE, Old „Path“: Путања радне фасцикле у којој се програм извршава. „Terminal“: Да ли се програм извршава у прозору терминала. 128тач 16тач 32тач 64тач <b>Појединости програма</b> <b>Преглед</b> <b>Изаберите слику</b> <big><b>Име програма</b></big> Мали описни сажетак о овом програму. О програму О Слободном Изборнику Алати Назив радње Радње Додај Додај фасциклу... Додај покретач... Додај раздвајач... Додај _фасциклу Додај _фасциклу... Додај _покретач Додај _покретач... Додај _раздвајач Додаје или уклања програме у изборнику Напредно Све измене од последњег сачуваног стања ће бити изгубљене и не могу се самостално повратити. Напомена програма Име програма Примени Да ли сте сигурни да желите избрисати „%s“? Да ли сте сигурни да желите избрисати овај раздвајач? Да ли сте сигурни да желите повратити овај покретач? Разгледај… Откажи Име врсте Очисти Наредба Права умножавања © 2012-2014 Sean Davis Избриши Опис Поставке радне површи Развој Да ли желите да читате упутство за Слободни Изборник на мрежи? Да ли желите да сачувате измене пре напуштања? Немој да сачуваш Образовање ГНОМ програми Корисничке поставке ГНОМ-а ГТК+ програми Игре Опште име Графика Поставке уређаја Помоћ Скривен Назив иконице Избор иконица Ако не сачувате покретач, све измене ће бити поништене. Ако не сачувате овај покретач, све измене ће бити изгубљене. Датотека слике Слике Интернет Кључне речи Уређивач изборника Слободан изборник Помери ниже Помери навише Име Нова фасцикла Нови покретач Нова ставка изборника Нова пречица Не приказуј у У реду Уред Упутство са мреже Само приказуј у Остало Напусти Читај са мреже Понови Уклони Поврати покретач Поврати Покрени у терминалу Сачувај Сачувај измене Сачувај покретач Тражи Излази претраге Тражи изразе... Изаберите врсту Изаберите радну фасциклу... Изаберите извршну датотеку... Изаберите иконицу... Изаберите слику Раздвајач Поставке Прикажи Приказуј поруке о грешкама Систем Ова ставка Ово је немогуће опозвано. Овај покретач је уклоњен са система.
Одређујем следећу доступну ставку. Пробај да извршиш Опозови Користи обавештења при покретању Корисничке поставке Вино Радна фасцикла Ставка изборника ИксФЦЕ-а Корисничке поставке ИксФЦЕ-а Немате овлашћења за брисање ове датотеке. Бићете преусмерени на веб страницу упутстава где се одржавају странице помоћи. _О програму _Додај раздвајач... _Садржај _Избриши _Уреди _Датотека _Помоћ _Напусти _Сачувај стубац 