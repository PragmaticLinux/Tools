��    *      l  ;   �      �     �     �  "   �  1   �       
   (     3     ;  	   G     Q     W     `  	   p  
   z     �     �  	   �  	   �     �  
   �     �     �     �     �     �     �     �     �     
               '     ,     3     8     Q     V  	   h     r     x     ~  �  �     #     4  "   F  #   i     �  
   �     �     �     �     �  	   �     �     �     �            	   $     .     ;  
   I     T     Y     k     {     �     �     �     �     �     �  	   �     �     �     �  #   �     	  
    	  
   +	     6	  	   ?	     I	                                                               #             *             	   "                                       (          
   &                  $   )          '   !      %              <b>Options</b> <b>Preview</b> <big><b>Application Name</b></big> A small descriptive blurb about this application. Accessories Categories Command Development Education Games Graphics Hide from menus Icon Name Image File Internet Menu Editor MenuLibre Move Down Move Up Multimedia Name New Menu Item New Shortcut Office Other Redo Revert Run in terminal Save Search Results Settings Show System Undo Use startup notification WINE Working Directory _Contents _Edit _File _Help Project-Id-Version: menulibre
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-08-03 13:05-0400
PO-Revision-Date: 2013-07-19 09:59+0000
Last-Translator: Pasi Lallinaho <pasi@shimmerproject.org>
Language-Team: Finnish <fi@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2014-08-04 06:22+0000
X-Generator: Launchpad (build 17147)
Language: fi
 <b>Asetukset</b> <b>Esikatselu</b> <big><b>Sovelluksen nimi</b></big> Lyhyt kuvaus tästä sovelluksesta. Apuohjelmat Kategoriat Komento Kehitystyökalut Opetusohjelmat Pelit Grafiikka Piilota valikoista Kuvakkeen nimi Kuvatiedosto Internet Valikkomuokkain MenuLibre Siirrä alas Siirrä ylös Multimedia Nimi Uusi valikkokohta Uusi pikakuvake Toimisto Muut Tee uudelleen Palauta ennalleen Aja päätteessä Tallenna Hakutulokset Asetukset Näytä Järjestelmä Kumoa Käytä käytnnistyksen huomautusta WINE Työkansio _Sisällys _Muokkaa _Tiedosto _Ohje 