��    /      �  C                   (     7  "   N  1   q     �     �     �     �  
   �     �     �  	   �     �     �       	          
   ,     7     @  	   L  	   V     `  
   h     s     x     �     �     �     �     �     �     �     �     �     �     �     �     �            	        $     *     0  �  6     �     �     �  '     ,   8  
   e     p     �  
   �  
   �     �     �  	   �     �     �     �     �     �     	     	     '	  	   :	  	   D	  	   N	     X	     d	     h	     }	  
   �	     �	     �	  	   �	     �	     �	     �	     �	     �	     �	     �	     
      
     %
  
   :
     E
     L
     T
                   )      (                "          .   +   
      	                 #                                    *          ,                              '                               $      &   %   !                   /   -    <b>Options</b> <b>Preview</b> <b>Select an image</b> <big><b>Application Name</b></big> A small descriptive blurb about this application. Accessories Add _Separator Apply Cancel Categories Command Development Education Games Graphics Hide from menus Icon Name Icon Selection Image File Internet Menu Editor MenuLibre Move Down Move Up Multimedia Name New Menu Item New Shortcut Office Other Redo Revert Run in terminal Save Search Results Settings Show System Undo Use startup notification WINE Working Directory _Contents _Edit _File _Help Project-Id-Version: menulibre
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-08-03 13:05-0400
PO-Revision-Date: 2014-07-15 00:07+0000
Last-Translator: Adolfo Jayme <fitoschido@gmail.com>
Language-Team: Catalan <ca@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2014-08-04 06:22+0000
X-Generator: Launchpad (build 17147)
Language: ca
 <b>Opcions</b> <b>Previsualització</b> <b>Trieu una imatge</b> <big><b>Nom de l’aplicació</b></big> Una descripció curta d’aquesta aplicació Accessoris Afegeix un _separador Aplica Cancel·la Categories Ordre Desenvolupament Educació Jocs Gràfics Oculta-ho dels menús Nom de la icona Selecció d’icones Fitxer d’imatge Internet Editor dels menús MenuLibre Mou avall Mou amunt Multimèdia Nom Element de menú nou Drecera nova Ofimàtica Altres Refés Reverteix Executa en terminal Desa Resultats de la cerca Configuració Mostra Sistema Desfés Usa una notificació d’inici WINE Directori de treball _Contingut _Edita _Fitxer _Ajuda 