# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-05-12 23:40+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../menulibre.desktop.in.h:1
msgid "Menu Editor"
msgstr ""

#: ../menulibre.desktop.in.h:2
msgid "Add or remove applications from the menu"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:1
msgid "Add _Launcher"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:2
msgid "Add _Directory"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:3
msgid "Add _Separator"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:4
msgid "Icon Selection"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:5 ../menulibre/MenulibreApplication.py:1042
#: ../menulibre/MenulibreApplication.py:1166
#: ../menulibre/MenulibreApplication.py:1291
#: ../menulibre/MenulibreApplication.py:1486
#: ../menulibre/MenulibreApplication.py:2682
#: ../menulibre/MenulibreApplication.py:2788
msgid "Cancel"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:6
msgid "Apply"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:7
msgid "Icon Name"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:8
msgid "Image File"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:9
msgid "<b>Select an image</b>"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:10
msgid "16px"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:11
msgid "32px"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:12
msgid "64px"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:13
msgid "128px"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:14
msgid "<b>Preview</b>"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:15 ../menulibre/MenulibreApplication.py:368
#: ../menulibre/MenulibreApplication.py:2811
msgid "MenuLibre"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:16
msgid "_File"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:17
msgid "_Edit"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:18
msgid "_Help"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:19
msgid "Save Launcher"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:20 ../menulibre/MenulibreApplication.py:420
msgid "Undo"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:21 ../menulibre/MenulibreApplication.py:427
msgid "Redo"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:22 ../menulibre/MenulibreApplication.py:434
msgid "Revert"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:23 ../menulibre/MenulibreApplication.py:441
msgid "Delete"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:24
msgid "Search terms…"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:25
msgid "Move Up"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:26
msgid "Move Down"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:27
msgid "<big><b>Application Name</b></big>"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:28
msgid "Application Name"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:29
msgid "Application Comment"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:30 ../menulibre/MenulibreApplication.py:848
msgid "Description"
msgstr ""

#. Please see http://standards.freedesktop.org/desktop-entry-spec/latest/ar01s05.html
#: ../data/ui/MenulibreWindow.ui.h:32
msgid ""
"\"Exec\": Program to execute, possibly with arguments. The Exec key is "
"required\n"
"if DBusActivatable is not set to true. Even if DBusActivatable is true, "
"Exec\n"
"should be specified for compatibility with implementations that do not\n"
"understand DBusActivatable. \n"
"\n"
"Please see\n"
"http://standards.freedesktop.org/desktop-entry-spec/desktop-entry-spec-"
"latest.html#exec-variables\n"
"for a list of supported arguments."
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:40
msgid "Command"
msgstr ""

#. Please see http://standards.freedesktop.org/desktop-entry-spec/latest/ar01s05.html
#: ../data/ui/MenulibreWindow.ui.h:42
msgid "\"Path\": The working directory to run the program in."
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:43
msgid "Working Directory"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:44
msgid "Browse…"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:45
msgid "<b>Application Details</b>"
msgstr ""

#. Please see http://standards.freedesktop.org/desktop-entry-spec/latest/ar01s05.html
#: ../data/ui/MenulibreWindow.ui.h:47
msgid "\"Terminal\": Whether the program runs in a terminal window."
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:48
msgid "Run in terminal"
msgstr ""

#. Please see http://standards.freedesktop.org/desktop-entry-spec/latest/ar01s05.html
#: ../data/ui/MenulibreWindow.ui.h:50
msgid ""
"\"StartupNotify\": If true, it is KNOWN that the application will send a "
"\"remove\"\n"
"message when started with the DESKTOP_STARTUP_ID environment variable set. "
"If\n"
"false, it is KNOWN that the application does not work with startup "
"notification\n"
"at all (does not shown any window, breaks even when using StartupWMClass, "
"etc.).\n"
"If absent, a reasonable handling is up to implementations (assuming false,\n"
"using StartupWMClass, etc.)."
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:56
msgid "Use startup notification"
msgstr ""

#. Please see http://standards.freedesktop.org/desktop-entry-spec/latest/ar01s05.html
#: ../data/ui/MenulibreWindow.ui.h:58
msgid ""
"\"NoDisplay\": NoDisplay means \"this application exists, but don't display "
"it in\n"
"the menus\". This can be useful to e.g. associate this application with "
"MIME\n"
"types, so that it gets launched from a file manager (or other apps), "
"without\n"
"having a menu entry for it (there are tons of good reasons for this,\n"
"including e.g. the netscape -remote, or kfmclient openURL kind of stuff)."
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:63
msgid "Hide from menus"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:64
msgid "<b>Options</b>"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:65
msgid "Add"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:66
msgid "Remove"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:67
msgid "Clear"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:68
msgid "Categories"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:69
msgid "Show"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:70
msgid "Name"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:71
msgid "Action Name"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:72
msgid "Actions"
msgstr ""

#. Please see http://standards.freedesktop.org/desktop-entry-spec/latest/ar01s05.html
#: ../data/ui/MenulibreWindow.ui.h:74
msgid ""
"\"GenericName\": Generic name of the application, for example \"Web Browser"
"\"."
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:75
msgid "Generic Name"
msgstr ""

#. Please see http://standards.freedesktop.org/desktop-entry-spec/latest/ar01s05.html
#: ../data/ui/MenulibreWindow.ui.h:77
msgid ""
"\"NotShowIn\": A list of strings identifying the environments that should "
"not\n"
"display a given desktop entry. Only one of these keys, either OnlyShowIn or\n"
"NotShowIn, may appear in a group.\n"
"\n"
"Possible values include: GNOME, KDE, LXDE, MATE, Razor, ROX, TDE, Unity, "
"XFCE, Old"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:82
msgid "Not Shown In"
msgstr ""

#. Please see http://standards.freedesktop.org/desktop-entry-spec/latest/ar01s05.html
#: ../data/ui/MenulibreWindow.ui.h:84
msgid ""
"\"OnlyShowIn\": A list of strings identifying the environments that should\n"
"display a given desktop entry. Only one of these keys, either OnlyShowIn or\n"
"NotShowIn, may appear in a group.\n"
"\n"
"Possible values include: GNOME, KDE, LXDE, MATE, Razor, ROX, TDE, Unity, "
"XFCE, Old"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:89
msgid "Only Shown In"
msgstr ""

#. Please see http://standards.freedesktop.org/desktop-entry-spec/latest/ar01s05.html
#: ../data/ui/MenulibreWindow.ui.h:91
msgid ""
"\"TryExec\": Path to an executable file on disk used to determine if the "
"program\n"
"is actually installed. If the path is not an absolute path, the file is "
"looked\n"
"up in the $PATH environment variable. If the file is not present or if it "
"is\n"
"not executable, the entry may be ignored (not be used in menus, for "
"example). "
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:95
msgid "Try Exec"
msgstr ""

#. Please see http://standards.freedesktop.org/desktop-entry-spec/latest/ar01s05.html
#: ../data/ui/MenulibreWindow.ui.h:97
msgid "\"MimeType\": The MIME type(s) supported by this application."
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:98
msgid "Mimetypes"
msgstr ""

#. Please see http://standards.freedesktop.org/desktop-entry-spec/latest/ar01s05.html
#: ../data/ui/MenulibreWindow.ui.h:100
msgid ""
"\"Keywords\": A list of strings which may be used in addition to other "
"metadata\n"
"to describe this entry. This can be useful e.g. to facilitate searching "
"through\n"
"entries. The values are not meant for display, and should not be redundant\n"
"with the values of Name or GenericName."
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:104
msgid "Keywords"
msgstr ""

#. Please see http://standards.freedesktop.org/desktop-entry-spec/latest/ar01s05.html
#: ../data/ui/MenulibreWindow.ui.h:106
msgid ""
"\"StartupWMClass\": If specified, it is known that the application will map "
"at\n"
"least one window with the given string as its WM class or WM name hint."
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:108
msgid "Startup WM Class"
msgstr ""

#. Please see http://standards.freedesktop.org/desktop-entry-spec/latest/ar01s05.html
#: ../data/ui/MenulibreWindow.ui.h:110
msgid ""
"\"Hidden\": Hidden should have been called Deleted. It means the user "
"deleted\n"
"(at his level) something that was present (at an upper level, e.g. in the\n"
"system dirs). It's strictly equivalent to the .desktop file not existing at\n"
"all, as far as that user is concerned. This can also be used to \"uninstall"
"\"\n"
"existing files (e.g. due to a renaming) - by letting make install install a\n"
"file with Hidden=true in it."
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:116
msgid "Hidden"
msgstr ""

#. Please see http://standards.freedesktop.org/desktop-entry-spec/latest/ar01s05.html
#: ../data/ui/MenulibreWindow.ui.h:118
msgid ""
"\"DBusActivatable\": A boolean value specifying if D-Bus activation is "
"supported\n"
"for this application. If this key is missing, the default value is false. "
"If\n"
"the value is true then implementations should ignore the Exec key and send "
"a\n"
"D-Bus message to launch the application. See D-Bus Activation for more\n"
"information on how this works. Applications should still include Exec= "
"lines\n"
"in their desktop files for compatibility with implementations that do not\n"
"understand the DBusActivatable key."
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:125
msgid "DBUS Activatable"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:126
msgid "Advanced"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:127
msgid "<small><i>Filename</i></small>"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:128
msgid "Select an icon…"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:129
msgid "<b>Select an icon</b>"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:130
msgid "Search"
msgstr ""

#: ../data/ui/MenulibreWindow.ui.h:131
msgid "column"
msgstr ""

#: ../menulibre/__init__.py:33
msgid "Show debug messages"
msgstr ""

#: ../menulibre/MenuEditor.py:86 ../menulibre/MenulibreApplication.py:1401
#: ../menulibre/MenulibreApplication.py:2254
msgid "Separator"
msgstr ""

#. Standard Items
#: ../menulibre/MenulibreApplication.py:59
msgid "Multimedia"
msgstr ""

#: ../menulibre/MenulibreApplication.py:60
msgid "Development"
msgstr ""

#: ../menulibre/MenulibreApplication.py:61
msgid "Education"
msgstr ""

#: ../menulibre/MenulibreApplication.py:62
msgid "Games"
msgstr ""

#: ../menulibre/MenulibreApplication.py:63
msgid "Graphics"
msgstr ""

#: ../menulibre/MenulibreApplication.py:64
msgid "Internet"
msgstr ""

#: ../menulibre/MenulibreApplication.py:65
msgid "Office"
msgstr ""

#: ../menulibre/MenulibreApplication.py:66
msgid "Settings"
msgstr ""

#: ../menulibre/MenulibreApplication.py:67
msgid "System"
msgstr ""

#: ../menulibre/MenulibreApplication.py:68
msgid "Accessories"
msgstr ""

#: ../menulibre/MenulibreApplication.py:69
msgid "WINE"
msgstr ""

#. Desktop Environment
#: ../menulibre/MenulibreApplication.py:71
msgid "Desktop configuration"
msgstr ""

#: ../menulibre/MenulibreApplication.py:72
msgid "User configuration"
msgstr ""

#: ../menulibre/MenulibreApplication.py:73
msgid "Hardware configuration"
msgstr ""

#. GNOME Specific
#: ../menulibre/MenulibreApplication.py:75
msgid "GNOME application"
msgstr ""

#: ../menulibre/MenulibreApplication.py:76
msgid "GTK+ application"
msgstr ""

#: ../menulibre/MenulibreApplication.py:77
msgid "GNOME user configuration"
msgstr ""

#: ../menulibre/MenulibreApplication.py:78
msgid "GNOME hardware configuration"
msgstr ""

#: ../menulibre/MenulibreApplication.py:79
#: ../menulibre/MenulibreApplication.py:80
msgid "GNOME system configuration"
msgstr ""

#. Xfce Specific
#: ../menulibre/MenulibreApplication.py:82
#: ../menulibre/MenulibreApplication.py:83
msgid "Xfce menu item"
msgstr ""

#: ../menulibre/MenulibreApplication.py:84
msgid "Xfce toplevel menu item"
msgstr ""

#: ../menulibre/MenulibreApplication.py:85
msgid "Xfce user configuration"
msgstr ""

#: ../menulibre/MenulibreApplication.py:86
msgid "Xfce hardware configuration"
msgstr ""

#: ../menulibre/MenulibreApplication.py:87
#: ../menulibre/MenulibreApplication.py:88
msgid "Xfce system configuration"
msgstr ""

#: ../menulibre/MenulibreApplication.py:136
#: ../menulibre/MenulibreApplication.py:181
msgid "Other"
msgstr ""

#: ../menulibre/MenulibreApplication.py:391
msgid "Add _Launcher..."
msgstr ""

#: ../menulibre/MenulibreApplication.py:392
msgid "Add Launcher..."
msgstr ""

#: ../menulibre/MenulibreApplication.py:398
msgid "Add _Directory..."
msgstr ""

#: ../menulibre/MenulibreApplication.py:399
msgid "Add Directory..."
msgstr ""

#: ../menulibre/MenulibreApplication.py:405
msgid "_Add Separator..."
msgstr ""

#: ../menulibre/MenulibreApplication.py:406
msgid "Add Separator..."
msgstr ""

#: ../menulibre/MenulibreApplication.py:412
msgid "_Save"
msgstr ""

#: ../menulibre/MenulibreApplication.py:413
#: ../menulibre/MenulibreApplication.py:1043
#: ../menulibre/MenulibreApplication.py:1487
msgid "Save"
msgstr ""

#: ../menulibre/MenulibreApplication.py:419
msgid "_Undo"
msgstr ""

#: ../menulibre/MenulibreApplication.py:426
msgid "_Redo"
msgstr ""

#: ../menulibre/MenulibreApplication.py:433
msgid "_Revert"
msgstr ""

#: ../menulibre/MenulibreApplication.py:440
msgid "_Delete"
msgstr ""

#: ../menulibre/MenulibreApplication.py:447
msgid "_Quit"
msgstr ""

#: ../menulibre/MenulibreApplication.py:448
#: ../menulibre/MenulibreApplication.py:2763
msgid "Quit"
msgstr ""

#: ../menulibre/MenulibreApplication.py:454
msgid "_Contents"
msgstr ""

#: ../menulibre/MenulibreApplication.py:455
#: ../menulibre/MenulibreApplication.py:2761
msgid "Help"
msgstr ""

#: ../menulibre/MenulibreApplication.py:461
msgid "_About"
msgstr ""

#: ../menulibre/MenulibreApplication.py:462
#: ../menulibre/MenulibreApplication.py:2762
msgid "About"
msgstr ""

#. Create a new column.
#: ../menulibre/MenulibreApplication.py:580
msgid "Search Results"
msgstr ""

#: ../menulibre/MenulibreApplication.py:822
msgid "ThisEntry"
msgstr ""

#: ../menulibre/MenulibreApplication.py:840
msgid "Select a category"
msgstr ""

#: ../menulibre/MenulibreApplication.py:843
msgid "Category Name"
msgstr ""

#: ../menulibre/MenulibreApplication.py:927
msgid "This Entry"
msgstr ""

#: ../menulibre/MenulibreApplication.py:982
msgid "New Shortcut"
msgstr ""

#. Unsaved changes
#: ../menulibre/MenulibreApplication.py:1032
msgid "Do you want to save the changes before closing?"
msgstr ""

#: ../menulibre/MenulibreApplication.py:1033
msgid "If you don't save the launcher, all the changes will be lost.'"
msgstr ""

#: ../menulibre/MenulibreApplication.py:1040
#: ../menulibre/MenulibreApplication.py:1484
msgid "Save Changes"
msgstr ""

#: ../menulibre/MenulibreApplication.py:1041
#: ../menulibre/MenulibreApplication.py:1485
msgid "Don't Save"
msgstr ""

#: ../menulibre/MenulibreApplication.py:1163
msgid "Select an image"
msgstr ""

#: ../menulibre/MenulibreApplication.py:1167
#: ../menulibre/MenulibreApplication.py:1292
msgid "OK"
msgstr ""

#: ../menulibre/MenulibreApplication.py:1169
msgid "Images"
msgstr ""

#: ../menulibre/MenulibreApplication.py:1284
msgid "Select a working directory..."
msgstr ""

#: ../menulibre/MenulibreApplication.py:1288
msgid "Select an executable..."
msgstr ""

#. Display a dialog saying this item is missing
#: ../menulibre/MenulibreApplication.py:1448
msgid "No Longer Installed"
msgstr ""

#: ../menulibre/MenulibreApplication.py:1449
msgid ""
"This launcher has been removed from the system.\n"
"Selecting the next available item."
msgstr ""

#: ../menulibre/MenulibreApplication.py:1475
msgid "Do you want to save the changes before leaving this launcher?"
msgstr ""

#: ../menulibre/MenulibreApplication.py:1477
msgid "If you don't save the launcher, all the changes will be lost."
msgstr ""

#: ../menulibre/MenulibreApplication.py:1716
msgid "You do not have permission to delete this file."
msgstr ""

#: ../menulibre/MenulibreApplication.py:2149
msgid "Cannot add subdirectories to preinstalled system paths."
msgstr ""

#: ../menulibre/MenulibreApplication.py:2176
msgid "New Launcher"
msgstr ""

#: ../menulibre/MenulibreApplication.py:2227
msgid "New Directory"
msgstr ""

#: ../menulibre/MenulibreApplication.py:2677
msgid "Are you sure you want to restore this launcher?"
msgstr ""

#: ../menulibre/MenulibreApplication.py:2683
#: ../menulibre/MenulibreApplication.py:2684
msgid "Restore Launcher"
msgstr ""

#: ../menulibre/MenulibreApplication.py:2685
msgid ""
"All changes since the last saved state will be lost and cannot be restored "
"automatically."
msgstr ""

#: ../menulibre/MenulibreApplication.py:2700
msgid "Are you sure you want to delete this separator?"
msgstr ""

#: ../menulibre/MenulibreApplication.py:2703
#, python-format
msgid "Are you sure you want to delete \"%s\"?"
msgstr ""

#: ../menulibre/MenulibreApplication.py:2705
msgid "This cannot be undone."
msgstr ""

#: ../menulibre/MenulibreApplication.py:2783
msgid "Do you want to read the MenuLibre manual online?"
msgstr ""

#: ../menulibre/MenulibreApplication.py:2789
msgid "Read Online"
msgstr ""

#: ../menulibre/MenulibreApplication.py:2790
msgid "Online Documentation"
msgstr ""

#: ../menulibre/MenulibreApplication.py:2791
msgid ""
"You will be redirected to the documentation website where the help pages are "
"maintained."
msgstr ""

#. Populate the AboutDialog with all the relevant details.
#: ../menulibre/MenulibreApplication.py:2810
msgid "About MenuLibre"
msgstr ""

#: ../menulibre/MenulibreApplication.py:2813
msgid "Copyright © 2012-2014 Sean Davis"
msgstr ""

#: ../menulibre/MenulibreXdg.py:53
msgid "New Menu Item"
msgstr ""

#: ../menulibre/MenulibreXdg.py:55
msgid "A small descriptive blurb about this application."
msgstr ""
